//
// windjammer: Wenjie's Personal Collection of R Functions
// Copyright (C) 2018-2021  Wenjie Wang <wang@wwenjie.org>
//
// This file is part of the R package windjammer.
//
// The R package windjammer is free software: You can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any later
// version (at your option). See the GNU General Public License at
// <https://www.gnu.org/licenses/> for details.
//
// The R package windjammer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//

#include <Rcpp.h>
// [[Rcpp::plugins(cpp11)]]


// compare double-precision numbers for almost equality
bool cpp_is_equal(double a, double b)
{
    double MaxRelDiff {std::numeric_limits<double>::epsilon()};
    // compute the difference.
    double diff = std::abs(a - b);
    a = std::abs(a);
    b = std::abs(b);
    // Find the largest
    double largest = (b > a) ? b : a;
    if (diff <= largest * MaxRelDiff) {
        return true;
    } else {
        return false;
    }
}

// compare for almost <, <=, >, and >=
bool cpp_is_lt(double a, double b)
{
    if (cpp_is_equal(a, b)) {
        return false;
    }
    double MaxRelDiff {std::numeric_limits<double>::epsilon()};
    // compute the difference.
    double diff = b - a;
    a = std::abs(a);
    b = std::abs(b);
    // Find the largest
    double largest = (b > a) ? b : a;
    if (diff > largest * MaxRelDiff) {
        return true;
    } else {
        return false;
    }
}

bool cpp_is_le(double a, double b)
{
    return cpp_is_equal(a, b) || cpp_is_lt(a, b);
}

bool cpp_is_gt(double a, double b)
{
    return ! cpp_is_le(a, b);
}

bool cpp_is_ge(double a, double b)
{
    return cpp_is_equal(a, b) || cpp_is_gt(a, b);
}


// vector version
// [[Rcpp::export]]
Rcpp::LogicalVector rcpp_is_equal(Rcpp::NumericVector a,
                                  Rcpp::NumericVector b)
{
    Rcpp::LogicalVector out(a.size());
    for (int i {0}; i < a.size(); ++i) {
        out(i) = cpp_is_equal(a(i), b(i));
    }
    return out;
}

// [[Rcpp::export]]
Rcpp::LogicalVector rcpp_is_lt(Rcpp::NumericVector a,
                               Rcpp::NumericVector b)
{
    Rcpp::LogicalVector out(a.size());
    for (int i {0}; i < a.size(); ++i) {
        out(i) = cpp_is_lt(a(i), b(i));
    }
    return out;
}

// [[Rcpp::export]]
Rcpp::LogicalVector rcpp_is_gt(Rcpp::NumericVector a,
                               Rcpp::NumericVector b)
{
    Rcpp::LogicalVector out(a.size());
    for (int i {0}; i < a.size(); ++i) {
        out(i) = cpp_is_gt(a(i), b(i));
    }
    return out;
}

// [[Rcpp::export]]
Rcpp::LogicalVector rcpp_is_ge(Rcpp::NumericVector a,
                               Rcpp::NumericVector b)
{
    Rcpp::LogicalVector out(a.size());
    for (int i {0}; i < a.size(); ++i) {
        out(i) = cpp_is_ge(a(i), b(i));
    }
    return out;
}

// [[Rcpp::export]]
Rcpp::LogicalVector rcpp_is_le(Rcpp::NumericVector a,
                               Rcpp::NumericVector b)
{
    Rcpp::LogicalVector out(a.size());
    for (int i {0}; i < a.size(); ++i) {
        out(i) = cpp_is_le(a(i), b(i));
    }
    return out;
}
