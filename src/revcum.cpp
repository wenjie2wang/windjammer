//
// windjammer: Wenjie's Personal Collection of R Functions
// Copyright (C) 2018-2021  Wenjie Wang <wang@wwenjie.org>
//
// This file is part of the R package windjammer.
//
// The R package windjammer is free software: You can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any later
// version (at your option). See the GNU General Public License at
// <https://www.gnu.org/licenses/> for details.
//
// The R package windjammer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//

// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::plugins(cpp11)]]
#include <RcppArmadillo.h>


// [[Rcpp::export]]
Rcpp::NumericVector revcumsum(const Rcpp::NumericVector &x)
{
  const long int n_x {x.size()};
  Rcpp::NumericVector res(n_x);
  double tmp {0.0};
  long int i {n_x};
  while (i > 0) {
    --i;
    tmp += x[i];
    res[i] = tmp;
  }
  return res;
}


// column-wise cumulative sum in possibly reverse order
// [[Rcpp::export]]
arma::mat colCumsums(const arma::mat& x,
                     const bool reversely = false)
{
  // if cumsum reversely
  if (reversely) {
    const unsigned long int n_x = x.n_rows;
    arma::rowvec tmp {arma::zeros(1, x.n_cols)};
    arma::mat res {x};
    for (size_t i {1}; i <= n_x; ++i) {
      tmp += x.row(n_x - i);
      res.row(n_x - i) = tmp;
    }
    return res;
  }
  // otherwise, using arma::cumsum
  return arma::cumsum(x, 0);
}
