#!/bin/bash

# Note: this script is should be sourced from the project root directory

set -e

if [[ "$(uname)" != "Linux" ]]; then
    printf "Sorry, this script is not intended for MacOS or other System.\n"
else
    printf "Removing copyright header from all scripts.\n"
    # remove copyright year in all R scripts
    for Rfile in R/*.R
    do
        if grep -q 'Copyright (C)' $Rfile; then
            sed -i "/^##$/,/^$/d" $Rfile
        fi
    done
    # update copyright year in all C++ scripts
    for cppfile in src/*.[hc]pp
    do
        if grep -q 'Copyright (C)' $cppfile; then
            sed -i "/^\/\/$/,/^$/d" $cppfile
        fi
    done
    printf "All removed.\n"
fi
