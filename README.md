# windjammer

A simple R package shipping a collection of tools for Wenjie's own usage.

- [Online documentation][homepage]


## Installation

Option 1: With the help of **remotes** or **devtools** package, we may easily
install it from R.

```R
remotes::install_gitlab("wenjie2wang/windjammer")
## devtools::install_gitlab("wenjie2wang/windjammer")
```

Option 2: It is also possible to install it in terminal.

```bash
git clone https://gitlab.com/wenjie2wang/windjammer.git
make install -C windjammer
```


[homepage]: https://wenjie2wang.gitlab.io/windjammer/

